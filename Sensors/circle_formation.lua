local sensorInfo = {
	name = "Sord units ids",
	desc = "Return table where [unitId]=order number",
	author = "Vojtech Sejkora",
	date = "2019-04-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups

local is_int = function(n)
  return (type(n) == "number") and (math.floor(n) == n)
end

-- @description Return table of Vec3 
-- @params around - Vec3
-- @params r - number radius of formation
-- @params number - how many point on circle
-- @return table | empty table
return function(around, r, number)
	if number <= 0 then
		return {}
	end
	
	local angle = 2 * math.pi / number
	local formation = {}
	
	for i=1, number do
		x = math.sin(angle*i) * r + around["x"]
		z = math.cos(angle*i) * r + around["z"]
		position = Vec3(x, around["y"], z)
		formation[i] = position
	end	
	return formation
end
