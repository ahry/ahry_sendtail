function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "positions", -- units to positions in formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<units to positions>",
			},
			{ 
				name = "formation", -- absolute position formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<absolute position formation>",
			},
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			}
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	if #units == 0 then
		return SUCCESS
	end
	local positions = parameter.positions -- table
	local formation = parameter.formation -- array of Vec3
	local fight = parameter.fight -- boolean
		
	-- validation
	
	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end	
	
	
	for i=1,#units do
		unitID = units[i]
		if not positions[unitID] then
			Logger.warn("Sendsail.move", "For unit [" .. unitID .. "] is not defined position [" .. dump(positions) .. "]") 
			return FAILURE
		end
		if not formation[positions[unitID]] then
			Logger.warn("Sendsail.move", "For position [" .. unitID .. "] is not defined formation point [" .. dump(formation) .. "]") 
			return FAILURE
		end
		local thisUnitWantedPosition = formation[positions[unitID]]
		SpringGiveOrderToUnit(unitID, cmdID, thisUnitWantedPosition:AsSpringVector(), {})
	end
	
	-- check success everyone is at position
	for i=1,#units do
		unitID = units[i]
		x, _, z = SpringGetUnitPosition(unitID)
		wantX, _, wantZ = formation[positions[unitID]]
		if x ~= wantX or z ~= wantZ then
			return RUNNING
		end
	end
	return SUCCESS
end